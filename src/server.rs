use std::fs::create_dir_all;
use std::path::Path;

use chrono::Utc;
use log::info;
use russh::*;

pub struct Server;

impl server::Server for Server {
    type Handler = crate::handler::Handler;
    fn new_client(&mut self, addr: Option<std::net::SocketAddr>) -> Self::Handler {
        let addr = if let Some(addr) = addr {
            format!("{}", addr)
        } else {
            format!("unknown")
        };
        info!("Connection from {}", addr);

        let path = &format!(
            "{}-{}.txt",
            Utc::now().format("%Y-%m-%d/%H:%M:%S"),
            addr
        );
        let path = Path::new(path);
        create_dir_all(path.parent().unwrap()).unwrap();
        crate::handler::Handler::new(path.display())
    }
}
