use std::str::FromStr;
use std::sync::Arc;

mod server;
mod handler;

#[tokio::main]
async fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .init();

    let listen_addr = std::env::args().skip(1).next()
        .expect("Expecting <addr:port>");

    let mut config = russh::server::Config::default();
    config.auth_banner = Some("#
# DEMONSTRATION SYSTEM
#
# All input will be published
#
");
    config.connection_timeout = Some(std::time::Duration::from_secs(10000));
    config.auth_rejection_time = std::time::Duration::from_secs(1);
    config.keys.push(russh_keys::key::KeyPair::generate_ed25519().unwrap());
    let config = Arc::new(config);
    let sh = server::Server;
    russh::server::run(
        config,
        &std::net::SocketAddr::from_str(&listen_addr).unwrap(),
        sh,
    )
    .await
    .unwrap();
}
