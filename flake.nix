{
  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, utils, fenix, naersk, ... }: {
    nixosModule = self.nixosModules.sshlogd;
    nixosModules.sshlogd = import ./nixos-module.nix { inherit self; };
  } //
  utils.lib.eachSystem (with utils.lib.system; [ x86_64-linux aarch64-linux ]) (system: let
    rust = fenix.packages.${system}.stable.withComponents [
      "cargo"
      "rustc"
      "rust-src"  # just for rust-analyzer
      "clippy"
    ];

    # Override the version used in naersk
    naersk-lib = naersk.lib."${system}".override {
      cargo = rust;
      rustc = rust;
    };

  in {
    defaultPackage = self.packages.${system}.sshlogd;

    packages.sshlogd = naersk-lib.buildPackage {
      src = ./.;
      meta.mainProgram = "sshlogd";
    };
  });
}
