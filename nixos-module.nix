{ self }:
{ config, lib, pkgs, ... }: {
  options.services.sshlogd = with lib; {
    enable = mkEnableOption "sshlogd";
    listenAddr = mkOption {
      type = types.str;
      default = "0.0.0.0";
    };
    listenPort = mkOption {
      type = types.port;
      default = 22;
    };
    outputDir = mkOption {
      type = types.path;
      default = "/var/lib/sshlogd";
    };
    user = mkOption {
      type = types.str;
      default = "sshlogd";
    };
    group = mkOption {
      type = types.str;
      default = "sshlogd";
    };
  };

  config =
    let
      cfg = config.services.sshlogd;
    in
      lib.mkIf cfg.enable {
        users = {
          users.${cfg.user} = {
            isSystemUser = true;
            group = cfg.group;
          };
          groups.${cfg.group} = {};
        };

        systemd.tmpfiles.rules = [
          "d ${cfg.outputDir} 0755 ${cfg.user} ${cfg.group} -"
        ];

        systemd.services.sshlogd = {
          wantedBy = [ "multi-user.target" ];
          after = [ "network.target" ];
          serviceConfig = {
            Type = "simple";
            User = cfg.user;
            Group = cfg.group;
            ExecStart = "${lib.getExe self.packages.${pkgs.stdenv.system}.sshlogd} '${cfg.listenAddr}:${toString cfg.listenPort}'";
            WorkingDirectory = cfg.outputDir;
            ReadWritePaths = cfg.outputDir;
            ProtectSystem = "full";
            Restart = "always";
            RestartSec = "60s";
            # Allow binding ports <1024
            AmbientCapabilities = "CAP_NET_BIND_SERVICE";
          };
        };
      };
}
